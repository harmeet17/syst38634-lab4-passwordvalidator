package time;

import static org.junit.Assert.*;

import org.junit.Test;
/**
 * 
 * @author harmeet kaur 991539912
 *
 */
public class TimeTest {

	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}

	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0A");
		fail("The time provided is not valid");
	}
	
	@Test 
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:59");
		assertTrue("The time provided does not match the result", totalSeconds == 59);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		fail("The time provided is not valid");
	}
	
	//test for getMilliseconds
	
	@Test
	public void testGetMillisecondsRegular() {
		int totalMilliseconds = Time.getMilliseconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetMillisecondsException() {
		int totalMilliseconds = Time.getMilliseconds("12:05:50:0A");
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getMilliseconds("12:05:50:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getMilliseconds("12:05:50:1000");
		fail("Invalid number of milliseconds");
	}

}
