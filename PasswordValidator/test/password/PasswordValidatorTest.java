package password;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author harmeet kaur 991539912
 * 
 * This class will use the TDD approach to validate the length of the password - at least 8 characters
 * and the number of digits - at least 2 digits.
 */
public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular()
	{
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("Harmeet"));
	}
	
	@Test
	public void testHasValidCaseCharsException()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("12345"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionEmpty()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower()
	{
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("a"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn()
	{
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("Aa"));
	}
	
	@Test
	public void testIsValidLengthRegular()
	{
		boolean passwordLength = PasswordValidator.IsvalidLength("TestingWorld");
		assertTrue("Invalid password length", passwordLength);
	}
	
	@Test
	public void testIsValidLengthException()
	{
		boolean passwordLength = PasswordValidator.IsvalidLength("Test");
		assertFalse("Invalid password length", passwordLength);
	}
	
	@Test
	public void testIsValidLengthExceptionSpace()
	{
		boolean passwordLength = PasswordValidator.IsvalidLength("Test Test");
		assertFalse("Invalid password length", passwordLength);
	}
	
	@Test
	public void testIsValidLengthExceptionNull()
	{
		boolean passwordLength = PasswordValidator.IsvalidLength(null);
		assertFalse("Invalid password length", passwordLength);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn()
	{
		boolean passwordLength = PasswordValidator.IsvalidLength("Testing1");
		assertTrue("Invalid password length", passwordLength);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut()
	{
		boolean passwordLength = PasswordValidator.IsvalidLength("Testing");
		assertFalse("Invalid password length", passwordLength);
	}
	
	//tests for hasValidDigitCount

	@Test
	public void testHasValidDigitCountRegular()
	{
		boolean isValidPassword = PasswordValidator.hasValidDigitCount("Testing123");
		assertTrue("Invalid digits count", isValidPassword);
	}
	
	@Test
	public void testHasValidDigitCountException()
	{
		boolean isValidPassword = PasswordValidator.hasValidDigitCount("Testings");
		assertFalse("Invalid digits count", isValidPassword);
	}
	
	@Test (expected=NullPointerException.class)
	public void testHasValidDigitCountExceptionNull()
	{
		boolean isValidPassword = PasswordValidator.hasValidDigitCount(null);
		assertFalse("Invalid digits count", isValidPassword);
	}
	
	@Test 
	public void testHasValidDigitCountExceptionSpace()
	{
		boolean passwordLength = PasswordValidator.hasValidDigitCount("Test 1234");
		assertFalse("Invalid password length", passwordLength);
	}
		
	@Test 
	public void testHasValidDigitCountBoundaryIn()
	{
		boolean passwordLength = PasswordValidator.hasValidDigitCount("Testing12");
		assertTrue("Invalid password length", passwordLength);
	}
	
	@Test 
	public void testHasValidDigitCountBoundaryOut()
	{
		boolean passwordLength = PasswordValidator.hasValidDigitCount("Testing1");
		assertFalse("Invalid password length", passwordLength);
	}
		
}
