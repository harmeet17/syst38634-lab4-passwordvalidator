package password;

/**
 * 
 * @author harmeet kaur 991539912
 * 
 * This class has two methods that validates the length of the password and counts the number of digits 
 * it contains and vaidates according to the given condition
 * Assume spaces should not be considered as valid characters for the purpose of calculating lengths
 */
public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	public static int MIN_COUNT = 2;
	
	public static boolean hasValidCaseChars(String password)
	{
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
	public static boolean IsvalidLength(String password)
	{
		return (password != null && !password.contains(" ") && password.length() >= MIN_LENGTH);
	}
	
	public static boolean hasValidDigitCount(String password) {
		int count = 0;
		for(int i = 0; i < password.length(); i++) {
			if (Character.isDigit(password.charAt(i)))
				count++;
		}
		
		if(count >= MIN_COUNT && !password.contains(" "))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
